import pandas as pd

def extract_title(name):
    import re
    title_search = re.search(r' (Mr(s|))+)\.', name)
    if title_search:
        return title_search.group(1)
    return ""

df['Title'] = df['Name'].apply(extract_title)

def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()

    df.groupby("Title").median()

